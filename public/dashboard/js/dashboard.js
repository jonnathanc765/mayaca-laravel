/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/dashboard/dashboard.js":
/*!*********************************************!*\
  !*** ./resources/js/dashboard/dashboard.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ( function ( $ ) {
//     "use strict";
// // const brandPrimary = '#20a8d8'
// const brandSuccess = '#4dbd74'
// const brandInfo = '#63c2de'
// const brandDanger = '#f86c6b'
// function convertHex (hex, opacity) {
//   hex = hex.replace('#', '')
//   const r = parseInt(hex.substring(0, 2), 16)
//   const g = parseInt(hex.substring(2, 4), 16)
//   const b = parseInt(hex.substring(4, 6), 16)
//   const result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')'
//   return result
// }
// function random (min, max) {
//   return Math.floor(Math.random() * (max - min + 1) + min)
// }
//     var elements = 27
//     var data1 = []
//     var data2 = []
//     var data3 = []
//     for (var i = 0; i <= elements; i++) {
//       data1.push(random(50, 200))
//       data2.push(random(80, 100))
//       data3.push(65)
//     }
//     //Traffic Chart
//     var ctx = document.getElementById( "trafficChart" );
//     //ctx.height = 200;
//     var myChart = new Chart( ctx, {
//         type: 'line',
//         data: {
//             labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S', 'M', 'T', 'W', 'T', 'F', 'S', 'S'],
//             datasets: [
//             {
//               label: 'My First dataset',
//               backgroundColor: convertHex(brandInfo, 10),
//               borderColor: brandInfo,
//               pointHoverBackgroundColor: '#fff',
//               borderWidth: 2,
//               data: data1
//           },
//           {
//               label: 'My Second dataset',
//               backgroundColor: 'transparent',
//               borderColor: brandSuccess,
//               pointHoverBackgroundColor: '#fff',
//               borderWidth: 2,
//               data: data2
//           },
//           {
//               label: 'My Third dataset',
//               backgroundColor: 'transparent',
//               borderColor: brandDanger,
//               pointHoverBackgroundColor: '#fff',
//               borderWidth: 1,
//               borderDash: [8, 5],
//               data: data3
//           }
//           ]
//         },
//         options: {
//             //   maintainAspectRatio: true,
//             //   legend: {
//             //     display: false
//             // },
//             // scales: {
//             //     xAxes: [{
//             //       display: false,
//             //       categoryPercentage: 1,
//             //       barPercentage: 0.5
//             //     }],
//             //     yAxes: [ {
//             //         display: false
//             //     } ]
//             // }
//             maintainAspectRatio: true,
//             legend: {
//                 display: false
//             },
//             responsive: true,
//             scales: {
//                 xAxes: [{
//                   gridLines: {
//                     drawOnChartArea: false
//                   }
//                 }],
//                 yAxes: [ {
//                       ticks: {
//                         beginAtZero: true,
//                         maxTicksLimit: 5,
//                         stepSize: Math.ceil(250 / 5),
//                         max: 250
//                       },
//                       gridLines: {
//                         display: true
//                       }
//                 } ]
//             },
//             elements: {
//                 point: {
//                   radius: 0,
//                   hitRadius: 10,
//                   hoverRadius: 4,
//                   hoverBorderWidth: 3
//               }
//           }
//         }
//     } );
// } )( jQuery );

/***/ }),

/***/ "./resources/js/dashboard/main.js":
/*!****************************************!*\
  !*** ./resources/js/dashboard/main.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$.noConflict();
jQuery(document).ready(function ($) {
  "use strict";

  [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
    new SelectFx(el);
  });
  jQuery('.selectpicker').selectpicker;
  $('#menuToggle').on('click', function (event) {
    $('body').toggleClass('open');
  });
  $('.search-trigger').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    $('.search-trigger').parent('.header-left').addClass('open');
  });
  $('.search-close').on('click', function (event) {
    event.preventDefault();
    event.stopPropagation();
    $('.search-trigger').parent('.header-left').removeClass('open');
  }); // $('.user-area> a').on('click', function(event) {
  // 	event.preventDefault();
  // 	event.stopPropagation();
  // 	$('.user-menu').parent().removeClass('open');
  // 	$('.user-menu').parent().toggleClass('open');
  // });
});

/***/ }),

/***/ "./resources/js/dashboard/widgets.js":
/*!*******************************************!*\
  !*** ./resources/js/dashboard/widgets.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ( function ( $ ) {
//     "use strict";
//     // Counter Number
//     $('.count').each(function () {
//         $(this).prop('Counter',0).animate({
//             Counter: $(this).text()
//         }, {
//             duration: 3000,
//             easing: 'swing',
//             step: function (now) {
//                 $(this).text(Math.ceil(now));
//             }
//         });
//     });
//     //WidgetChart 1
//     var ctx = document.getElementById( "widgetChart1" );
//     ctx.height = 150;
//     var myChart = new Chart( ctx, {
//         type: 'line',
//         data: {
//             labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//             type: 'line',
//             datasets: [ {
//                 data: [65, 59, 84, 84, 51, 55, 40],
//                 label: 'Dataset',
//                 backgroundColor: 'transparent',
//                 borderColor: 'rgba(255,255,255,.55)',
//             }, ]
//         },
//         options: {
//             maintainAspectRatio: false,
//             legend: {
//                 display: false
//             },
//             responsive: true,
//             scales: {
//                 xAxes: [ {
//                     gridLines: {
//                         color: 'transparent',
//                         zeroLineColor: 'transparent'
//                     },
//                     ticks: {
//                         fontSize: 2,
//                         fontColor: 'transparent'
//                     }
//                 } ],
//                 yAxes: [ {
//                     display:false,
//                     ticks: {
//                         display: false,
//                     }
//                 } ]
//             },
//             title: {
//                 display: false,
//             },
//             elements: {
//                 line: {
//                     borderWidth: 1
//                 },
//                 point: {
//                     radius: 4,
//                     hitRadius: 10,
//                     hoverRadius: 4
//                 }
//             }
//         }
//     } );
//     //WidgetChart 2
//     var ctx = document.getElementById( "widgetChart2" );
//     ctx.height = 150;
//     var myChart = new Chart( ctx, {
//         type: 'line',
//         data: {
//             labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//             type: 'line',
//             datasets: [ {
//                 data: [1, 18, 9, 17, 34, 22, 11],
//                 label: 'Dataset',
//                 backgroundColor: '#63c2de',
//                 borderColor: 'rgba(255,255,255,.55)',
//             }, ]
//         },
//         options: {
//             maintainAspectRatio: false,
//             legend: {
//                 display: false
//             },
//             responsive: true,
//             scales: {
//                 xAxes: [ {
//                     gridLines: {
//                         color: 'transparent',
//                         zeroLineColor: 'transparent'
//                     },
//                     ticks: {
//                         fontSize: 2,
//                         fontColor: 'transparent'
//                     }
//                 } ],
//                 yAxes: [ {
//                     display:false,
//                     ticks: {
//                         display: false,
//                     }
//                 } ]
//             },
//             title: {
//                 display: false,
//             },
//             elements: {
//                 line: {
//                     tension: 0.00001,
//                     borderWidth: 1
//                 },
//                 point: {
//                     radius: 4,
//                     hitRadius: 10,
//                     hoverRadius: 4
//                 }
//             }
//         }
//     } );
//     //WidgetChart 3
//     var ctx = document.getElementById( "widgetChart3" );
//     ctx.height = 70;
//     var myChart = new Chart( ctx, {
//         type: 'line',
//         data: {
//             labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
//             type: 'line',
//             datasets: [ {
//                 data: [78, 81, 80, 45, 34, 12, 40],
//                 label: 'Dataset',
//                 backgroundColor: 'rgba(255,255,255,.2)',
//                 borderColor: 'rgba(255,255,255,.55)',
//             }, ]
//         },
//         options: {
//             maintainAspectRatio: true,
//             legend: {
//                 display: false
//             },
//             responsive: true,
//             scales: {
//                 xAxes: [ {
//                     gridLines: {
//                         color: 'transparent',
//                         zeroLineColor: 'transparent'
//                     },
//                     ticks: {
//                         fontSize: 2,
//                         fontColor: 'transparent'
//                     }
//                 } ],
//                 yAxes: [ {
//                     display:false,
//                     ticks: {
//                         display: false,
//                     }
//                 } ]
//             },
//             title: {
//                 display: false,
//             },
//             elements: {
//                 line: {
//                     borderWidth: 2
//                 },
//                 point: {
//                     radius: 0,
//                     hitRadius: 10,
//                     hoverRadius: 4
//                 }
//             }
//         }
//     } );
//     //WidgetChart 4
//     var ctx = document.getElementById( "widgetChart4" );
//     ctx.height = 70;
//     var myChart = new Chart( ctx, {
//         type: 'bar',
//         data: {
//             labels: ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
//             datasets: [
//                 {
//                     label: "My First dataset",
//                     data: [78, 81, 80, 45, 34, 12, 40, 75, 34, 89, 32, 68, 54, 72, 18, 98],
//                     borderColor: "rgba(0, 123, 255, 0.9)",
//                     //borderWidth: "0",
//                     backgroundColor: "rgba(255,255,255,.3)"
//                 }
//             ]
//         },
//         options: {
//               maintainAspectRatio: true,
//               legend: {
//                 display: false
//             },
//             scales: {
//                 xAxes: [{
//                   display: false,
//                   categoryPercentage: 1,
//                   barPercentage: 0.5
//                 }],
//                 yAxes: [ {
//                     display: false
//                 } ]
//             }
//         }
//     } );
// } )( jQuery );

/***/ }),

/***/ 1:
/*!************************************************************************************************************************!*\
  !*** multi ./resources/js/dashboard/dashboard.js ./resources/js/dashboard/main.js ./resources/js/dashboard/widgets.js ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\laragon\www\mayaca\resources\js\dashboard\dashboard.js */"./resources/js/dashboard/dashboard.js");
__webpack_require__(/*! C:\laragon\www\mayaca\resources\js\dashboard\main.js */"./resources/js/dashboard/main.js");
module.exports = __webpack_require__(/*! C:\laragon\www\mayaca\resources\js\dashboard\widgets.js */"./resources/js/dashboard/widgets.js");


/***/ })

/******/ });