<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();

        $client = factory(App\User::class)->create([
            'email' => 'client@gmail.com'
        ]);
        $client->assignRole('client');

        $admin = factory(App\User::class)->create([
            'email' => 'admin@gmail.com'
        ]);
        $admin->assignRole('admin');

        $driver = factory(App\User::class)->create([
            'email' => 'driver@gmail.com'
        ]);
        $driver->assignRole('driver');

        $jonna = factory(App\User::class)->create([
            'email' => 'jonna@gmail.com'
        ]);
        $jonna->assignRole('super-admin');



    }
}
