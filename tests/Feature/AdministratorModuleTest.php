<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdministratorModuleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_module_is_load()
    {
        $response = $this->get('/administrator');
        $response->assertStatus(200)
                ->assertSee('Dashboard')
                ->assertSee('Mi Perfil');
    }
}
