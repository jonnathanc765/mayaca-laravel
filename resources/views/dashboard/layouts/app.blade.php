<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="es">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MayacaExpress - @yield('title')</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

   

    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">


    <link rel="stylesheet" href="{{ asset('dashboard/css/style.css') }}">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body class="@yield('body-class')">

    @section('body')
    
        @include('dashboard.layouts.left-sidebar')
    
        <!-- Right Panel -->
    
        <div id="right-panel" class="right-panel">
    
            @include('dashboard.layouts.header')
    
            <div class="breadcrumbs">
                <div class="col-sm-4">
                    <div class="page-header float-left">
                        <div class="page-title">
                            <h1>Dashboard</h1>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="page-header float-right">
                        <div class="page-title">
                            <ol class="breadcrumb text-right">
                                <li class="active">Dashboard</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="content mt-3">
    
                @yield('content')
    
            </div> <!-- .content -->
        </div><!-- /#right-panel -->
    
        <!-- Right Panel -->
        
    @show


    {{-- Bootstrap, jQuery and Popper --}}
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- Main js --}}
    <script src="{{ asset('dashboard/js/dashboard.js') }}"></script>  

</body>

</html>
