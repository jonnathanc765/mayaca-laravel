<h2>
    Pagina principal
</h2>

@auth
    <a href="{{ route('administrator.index') }}"></a>
@else
    <a href="{{ route('login') }}">Entrar</a>
    <a href="{{ route('register') }}">Registrarse</a>
@endauth